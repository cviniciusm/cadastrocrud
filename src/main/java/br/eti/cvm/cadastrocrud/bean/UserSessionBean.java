package br.eti.cvm.cadastrocrud.bean;

import java.io.Serializable;
import java.util.ResourceBundle;

import javax.enterprise.context.SessionScoped;
import javax.faces.annotation.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.keycloak.KeycloakSecurityContext;

import lombok.extern.jbosslog.JBossLog;

@Named
@SessionScoped
@JBossLog
public class UserSessionBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	@ManagedProperty("#{label}")
	private ResourceBundle label;

	@Inject
	private ExternalContext externalContext;

	public String getUserName() {

		try {
			HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();

			KeycloakSecurityContext ksc = (KeycloakSecurityContext) request
					.getAttribute(KeycloakSecurityContext.class.getName());

			return ksc != null ? ksc.getToken().getPreferredUsername() : "";
		} catch (Throwable e) {
//			log.error("Não foi possível obter o usuário logado", e);
		}

		return null;
	}

	public String getFullName() {

		try {
			HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();

			KeycloakSecurityContext ksc = (KeycloakSecurityContext) request
					.getAttribute(KeycloakSecurityContext.class.getName());

			return ksc != null ? ksc.getToken().getName() : "";
		} catch (Throwable e) {
//			log.error("Não foi possível obter o usuário logado", e);
		}

		return null;
	}
}
