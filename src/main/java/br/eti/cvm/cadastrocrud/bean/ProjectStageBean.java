package br.eti.cvm.cadastrocrud.bean;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.Application;
import javax.faces.application.ProjectStage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@ApplicationScoped
public class ProjectStageBean {
	private Boolean isNotProduction = Boolean.FALSE;

	@Inject
	private FacesContext facesContext;

	@PostConstruct
	private void init() {
		Application app = facesContext.getApplication();
		if (app != null) {
			ProjectStage stage = app.getProjectStage();
			if (!ProjectStage.Production.equals(stage)) {
				this.isNotProduction = Boolean.TRUE;
			}
		}
	}

	public Boolean isNotProduction() {
		return this.isNotProduction;
	}
}
