package br.eti.cvm.cadastrocrud.bean.crud;

import java.io.Serializable;
import java.util.ResourceBundle;

import javax.faces.annotation.ManagedProperty;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;

import br.eti.cvm.cadastrocrud.model.Person;
import br.eti.cvm.cadastrocrud.service.LogService;
import br.eti.cvm.cadastrocrud.service.PersonService;
import lombok.Getter;
import lombok.Setter;

@Named
@ViewScoped
public class UpdateBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	@Inject
	@ManagedProperty("#{flash.keep.person}")
	private Person person;

	@Inject
	private PersonService personService;

	@Inject
	private Flash flash;

	@Inject
	private FacesContext facesContext;

	@Inject
	@ManagedProperty("#{label}")
	private ResourceBundle label;

	@Inject
	private LogService logService;

	public String updateAction() {
		logService.logInfo("método atualizarAction foi chamado");

		try {
			personService.update(person);
		} catch (OptimisticLockException e) {
			facesContext.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, label.getString("optimisticlock"), null));
			return null;
		}

		flash.setKeepMessages(true);

		facesContext.addMessage(null, new FacesMessage(label.getString("recordupdatedsuccessfully")));

		return "retrieve?faces-redirect=true";
	}

	public void validateEmailDuplicated(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
		logService.logInfo("método validateEmailRepetido foi chamado");

		String email = (String) value;
		Person personRetrieved = personService.findByEmail(email);
		if ((personRetrieved != null) && !person.getEmail().equals(personRetrieved.getEmail())) {
			throw new ValidatorException(
					new FacesMessage(FacesMessage.SEVERITY_ERROR, null, label.getString("emailcannotbeduplicated")));
		}

		return;
	}

}