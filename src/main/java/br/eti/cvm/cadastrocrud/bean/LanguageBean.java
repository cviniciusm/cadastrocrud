package br.eti.cvm.cadastrocrud.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import lombok.Getter;
import lombok.Setter;

@Named
@SessionScoped
public class LanguageBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private FacesContext facesContext;

	@Getter
	private List<Locale> available;

	@Getter
	@Setter
	private Locale current;

	@PostConstruct
	private void init() {
		Application app = facesContext.getApplication();
		current = app.getViewHandler().calculateLocale(facesContext);
		available = new ArrayList<>();
		available.add(app.getDefaultLocale());
		app.getSupportedLocales().forEachRemaining(available::add);
	}

	public void reload() {
		facesContext.getPartialViewContext().getEvalScripts().add("location.replace(location)");
	}

	public String getLanguageTag() {
		return current.toLanguageTag();
	}

	public void setLanguageTag(String languageTag) {
		current = Locale.forLanguageTag(languageTag);
	}
}
