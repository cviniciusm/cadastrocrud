package br.eti.cvm.cadastrocrud.bean;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Named
@SessionScoped
public class SessionLogoutBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private ExternalContext externalContext;

	public String logout() {
		HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
		try {
			request.logout();
		} catch (ServletException e) {
			e.printStackTrace();
		}

		return "retrieve?faces-redirect=true";
	}
}
