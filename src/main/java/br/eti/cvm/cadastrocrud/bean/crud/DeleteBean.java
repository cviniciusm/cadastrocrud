package br.eti.cvm.cadastrocrud.bean.crud;

import java.util.ResourceBundle;

import javax.enterprise.context.RequestScoped;
import javax.faces.annotation.ManagedProperty;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.transaction.Transactional;

import br.eti.cvm.cadastrocrud.model.Person;
import br.eti.cvm.cadastrocrud.service.LogService;
import br.eti.cvm.cadastrocrud.service.PersonService;
import lombok.Getter;
import lombok.Setter;

@Named
@RequestScoped
public class DeleteBean {

	@Getter
	@Setter
	@Inject
	@ManagedProperty("#{flash.keep.person}")
	private Person person;

	@Inject
	private PersonService personService;

	@Inject
	private Flash flash;

	@Inject
	private FacesContext facesContext;

	@Inject
	@ManagedProperty("#{label}")
	private ResourceBundle label;

	@Inject
	private LogService logService;

	@Transactional
	public String deleteAction() {
		logService.logInfo("método excluirAction foi chamado");

		try {
			Person managed = personService.update(person);
			personService.delete(managed);
		} catch (OptimisticLockException e) {
			facesContext.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, label.getString("optimisticlock"), null));
			return null;
		}

		flash.setKeepMessages(true);
		facesContext.addMessage(null, new FacesMessage(label.getString("recorddeletedsuccessfully")));

		return "retrieve?faces-redirect=true";
	}

}