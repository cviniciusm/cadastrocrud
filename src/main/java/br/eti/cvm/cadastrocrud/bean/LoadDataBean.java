package br.eti.cvm.cadastrocrud.bean;

import java.util.Random;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.omnifaces.cdi.Startup;

import br.eti.cvm.cadastrocrud.model.Person;
import br.eti.cvm.cadastrocrud.model.PersonDetails;

@Startup
public class LoadDataBean {

	@PostConstruct
	private void init() {

		Random random = new Random();

		String[] randomWords = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto",
				"Setembro", "Outubro", "Novembro", "Dezembro", "Domingo", "Segunda", "Terça", "Quarta", "Quinta",
				"Sexta", "Sabado", "Manha", "Tarde", "Noite", "Sol", "Lua", "Chuva", "Vento", "Fogo", "Terra", "Gelo",
				"Branco", "Preto", "Azul", "Verde", "Vermelho", "Marrom", "Cinza", "Laranja", "Amarelo", "Uva", "Maçã",
				"Pera", "Abacaxi", "Kiwi", "Banana", "Limao", "Ameixa", "Damasco" };

		int length = randomWords.length;
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("cadastrocrud-persistence-unit");
		EntityManager em = factory.createEntityManager();

		for (int i = 0; i < 15; i++) {
			String email = randomWords[random.nextInt(length)] + "@" + randomWords[random.nextInt(length)] + "."
					+ randomWords[random.nextInt(length)];

			PersonDetails details = PersonDetails.builder()
					.age(random.nextInt(length))
					.address(randomWords[random.nextInt(length)])
					.city(randomWords[random.nextInt(length)])
					.zipCode(String.valueOf(70000 + 100 * random.nextInt(length)))
					.telephone(String.valueOf(random.nextInt(length)))
					.info(randomWords[random.nextInt(length)])
					.build();

			Person person = Person.builder()
					.firstName(randomWords[random.nextInt(length)])
					.lastName(randomWords[random.nextInt(length)])
					.email(email)
					.details(details)
					.build();
			
			em.getTransaction().begin();
			
			em.persist(person);
			
			em.getTransaction().commit();
		}
		
		em.close();
		factory.close();
	}
}
