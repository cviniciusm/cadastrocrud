package br.eti.cvm.cadastrocrud.bean;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import lombok.Getter;
import lombok.Setter;

@Named
@SessionScoped
public class ThemeBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Getter
	@Setter
	private String theme = "omega";

	public List<String> getThemes() {
		return Arrays.asList("afterdark", "afternoon", "afterwork", "aristo", "black-tie", "blitzer", "bluesky",
				"bootstrap", "casablanca", "cruze", "cupertino", "dark-hive", "delta", "dot-luv", "eggplant",
				"excite-bike", "flick", "glass-x", "home", "hot-sneaks", "humanity", "le-frog", "midnight", "mint-choc",
				"omega", "overcast", "pepper-grinder", "redmond", "rocket", "sam", "smoothness", "south-street",
				"start", "sunny", "swanky-purse", "trontastic", "ui-darkness", "ui-lightness", "vader");
	}
}
