package br.eti.cvm.cadastrocrud.service;

import java.io.Serializable;

import javax.inject.Inject;

import br.eti.cvm.cadastrocrud.bean.ProjectStageBean;
import lombok.extern.jbosslog.JBossLog;

@JBossLog
public class LogService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private ProjectStageBean projectStage;

	public void logInfo(String info) {
		if (projectStage.isNotProduction())
			log.info(info);
	}
}
