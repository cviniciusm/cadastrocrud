package br.eti.cvm.cadastrocrud.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CacheRetrieveMode;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.eti.cvm.cadastrocrud.model.Person;
import br.eti.cvm.cadastrocrud.model.Person_;

public class PersonRepository extends Repository<Person, Long> {

	private static final long serialVersionUID = 1L;

	public PersonRepository() {
		super(Person.class);
	}

	public Person findByEmail(String email) {
		TypedQuery<Person> query = em.createQuery("SELECT DISTINCT p FROM Person p WHERE p.email = :email",
				Person.class);
		query.setParameter("email", email);
		query.setHint("org.hibernate.cacheable", "true");
		query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.USE);

		Person person = null;
		try {
			person = query.getSingleResult();
		} catch (NoResultException e) {
		}

		return person;
	}

	public List<Person> searchByFullNameEmail(String name, String lastName, String email, Integer startPosition,
			Integer maxResult) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Person> criteria = cb.createQuery(Person.class);
		Root<Person> root = criteria.from(Person.class);

		Path<String> pathFirstName = root.get(Person_.firstName);
		Path<String> pathLastName = root.get(Person_.lastName);
		Path<String> pathEmail = root.get(Person_.email);

		List<Predicate> predicates = new ArrayList<>();

		if (!name.isEmpty()) {
			Predicate predicate = cb.like(cb.upper(pathFirstName), "%" + name.toUpperCase() + "%");
			predicates.add(predicate);
		}

		if (!lastName.isEmpty()) {
			Predicate predicate = cb.like(cb.upper(pathLastName), "%" + lastName.toUpperCase() + "%");
			predicates.add(predicate);
		}

		if (!email.isEmpty()) {
			Predicate predicate = cb.like(cb.upper(pathEmail), "%" + email.toUpperCase() + "%");
			predicates.add(predicate);
		}

		criteria.where((Predicate[]) predicates.toArray(new Predicate[0]));

		TypedQuery<Person> query = em.createQuery(criteria);

		query.setFirstResult(startPosition);
		query.setMaxResults(maxResult);
		query.setHint("org.hibernate.cacheable", "true");
		query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.USE);

		return query.getResultList();
	}

}
