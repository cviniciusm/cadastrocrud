package br.eti.cvm.cadastrocrud.repository;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.persistence.CacheRetrieveMode;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;
import javax.validation.Valid;

import br.eti.cvm.cadastrocrud.producer.EntityMangerQualifier;

public abstract class Repository<T, I extends Serializable> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	@EntityMangerQualifier
	protected EntityManager em;

	private Class<T> persistedClass;

	protected Repository() {
	}

	protected Repository(Class<T> persistedClass) {
		this();
		this.persistedClass = persistedClass;
	}

	@Transactional
	public T create(@Valid T entity) {
		em.persist(entity);
		return entity;
	}

	@Transactional
	public T update(@Valid T entity) {
		return em.merge(entity);
	}

	@Transactional
	public void delete(@Valid T entity) {
		em.remove(entity);
	}

	public List<T> retrieveAll() {
		CriteriaBuilder builder = em.getCriteriaBuilder();

		CriteriaQuery<T> query = builder.createQuery(persistedClass);

		query.from(persistedClass);

		TypedQuery<T> typedQuery = em.createQuery(query);

		typedQuery.setHint("org.hibernate.cacheable", "true");
		typedQuery.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.USE);

		return typedQuery.getResultList();
	}

	public T retrieve(I id) {
		Map<String, Object> props = new HashMap<String, Object>();
		props.put("org.hibernate.cacheable", "true");
		props.put("javax.persistence.cache.retrieveMode", CacheRetrieveMode.USE);

		return em.find(persistedClass, id, props);
	}

	public Long count() {
		CriteriaBuilder qb = em.getCriteriaBuilder();

		CriteriaQuery<Long> cq = qb.createQuery(Long.class);

		cq.select(qb.count(cq.from(persistedClass)));

		return em.createQuery(cq).getSingleResult();
	}

}