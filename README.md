# Cadastro CRUD

[![Known Vulnerabilities](https://snyk.io/test/github/cassiusvm/cadastrocrud/badge.svg?targetFile=pom.xml)](https://snyk.io/test/github/cassiusvm/cadastrocrud?targetFile=pom.xml)

Sample application that does a registration (performs CRUD operations)

## Tecnologies/frameworks/tools

* IDE Eclipse 2019-16
* Wildfly 16
* Primefaces 7.0
* Omnifaces 3.2
* Project Lombok 1.18.8
* JavaServer Faces 2.3
* Open JDK 11
* Keycloak 6.0+
* Hibernate 5.3
* JPA 2.2
* MariaDB 10.1

## MariaDB Configuration
```
<datasource
	jndi-name="java:jboss/datasources/cadastrocrudDS"
	pool-name="cadastrocrudDS" enabled="true"
	use-java-context="true">
	<connection-url>jdbc:mariadb://host:3306/bancocadastrocrud?useSSL=false</connection-url>
	<driver>mariadb</driver>
	<security>
		<user-name>cadastrocruduser</user-name>
		<password>cadastrocruduser</password>
	</security>
</datasource>
```
```
<driver name="mariadb" module="org.mariadb">
	<driver-class>org.mariadb.jdbc.Driver</driver-class>
</driver>
```
wildfly-16.0.0.Final\modules\system\layers\base\org\mariadb\main :

module.xml

[mariadb-java-client-2.4.2.jar](https://downloads.mariadb.com/Connectors/java/connector-java-2.4.2/mariadb-java-client-2.4.2.jar)

module.xml:

```
<?xml version="1.0" encoding="UTF-8"?>
<module xmlns="urn:jboss:module:1.5" name="org.mariadb">
    <resources>
        <resource-root path="mariadb-java-client-2.4.2.jar"/>
    </resources>
    <dependencies>
        <module name="javax.api"/>
        <module name="javax.transaction.api"/>
        <module name="javax.servlet.api" optional="true"/>
    </dependencies>
</module>
```

## PostgreSQL Configuration
```
<datasource jndi-name="java:jboss/datasources/cadastrocrudDS" pool-name="cadastrocrudDS" enabled="true" use-java-context="true">
	<connection-url>jdbc:postgresql:/host:5432/bancocadastrocrud</connection-url>
	<driver>postgresql</driver>
	<security>
		<user-name><!-- coloque o usuário do banco aqui --></user-name>
		<password><!-- coloque a senha do usuario aqui --></password>
	</security>
</datasource>
```
```
<driver name="postgresql" module="org.postgresql">
	<!-- for xa datasource -->
	<xa-datasource-class>org.postgresql.xa.PGXADataSource</xa-datasource-class>
	<!-- for non-xa datasource -->
	<driver-class>org.postgresql.Driver</driver-class>
</driver>
```
wildfly-13.0.0.Final\modules\system\layers\base\org\postgresql\main :

module.xml

[postgresql-42.2.2.jar](https://jdbc.postgresql.org/download.html)

module.xml:

```
<?xml version="1.0" encoding="UTF-8"?>
<module xmlns="urn:jboss:module:1.5" name="org.postgresql">
    <resources>
        <resource-root path="postgresql-42.2.2.jar"/>
    </resources>
    <dependencies>
        <module name="javax.api"/>
        <module name="javax.transaction.api"/>
    </dependencies>
</module>
```